from pickle import dump, load
from .chain import Blockchain, Transaction, InvalidBlockchainException, InvalidBlockException, uniq_id, DB0
from .apptypes import *
from merkletools import MerkleTools


TRANSACTIONS = ["add-bdomain", "update-bdomain", "delete-bdomain", 
                "add-bobject", "update-bobject", "delete-bobject"
                "add-group", "update-group", "delete-group",
                "add-user", "update-user", "delete-user",
                "add-acl", "update-acl", "delete-acl",
                "add-aci", "update-aci", "delete-aci",
]


class ACTION_DATA:
    def __init__(self, **kwargs):
        self.data = kwargs

    @property
    def hash(self):
        return hash(str(self.data.items()))


class Manager:

    def __init__(self):
        self.bdomainschain = Blockchain()
        self.bobjectschain = Blockchain()
        self.bdomainstree = MerkleTools(hash_type="md5")
        self.bobjectstree = MerkleTools(hash_type="md5")
        self.db0domains = DB0()
        self.db0objects = DB0()
        self.users = {}
        self.groups = {}
        self.acls = {}
        self.acis = {}

    def check_add_bdomain_tx(self, tx):
        try:
            return BDomain(**tx.tdata.data)
        except:
            return False

    def make_add_bdomain_tx(self, **kwargs):
        t = Transaction("add-bdomain", ACTION_DATA(**kwargs))
        return t

    def check_delete_bdomain_tx(self, tx):
        domuid = tx.tdata.data['uid']
        return self.db0domains.get(domuid, False)

    def make_delete_bdomain_tx(self, **kwargs):
        t = Transaction("delete-bdomain", ACTION_DATA(**kwargs))
        return t

    def check_update_bdomain_tx(self, tx):
        domuid = tx.tdata.data['uid']
        return self.db0domains.get(domuid, False)

    def make_update_bdomain_tx(self, **kwargs):
        t = Transaction("update-bdomain", ACTION_DATA(**kwargs))
        return t

    def check_add_bobject_tx(self, tx):
        try:
            return BObject(**tx.tdata.data)
        except:
            return False

    def make_add_bobject_tx(self, **kwargs):
        t = Transaction("add-bobject", ACTION_DATA(**kwargs))
        return t


    def make_add_user_tx(self, **kwargs):
        t = Transaction("add-user", ACTION_DATA(**kwargs))
        return t

    def make_add_group_tx(self, **kwargs):
        t = Transaction("add-group", ACTION_DATA(**kwargs))
        return t

    def make_add_acl_tx(self, **kwargs):
        t = Transaction("add-acl", ACTION_DATA(**kwargs))
        return t

    def make_add_aci_tx(self, **kwargs):
        t = Transaction("add-aci", ACTION_DATA(**kwargs))
        return t

    def check_delete_bobject_tx(self, tx):
        domuid = tx.tdata.data['uid']
        return self.db0objects.get(domuid, False)

    def make_delete_user_tx(self, **kwargs):
        t = Transaction("delete-user", ACTION_DATA(**kwargs))
        return t

    def make_delete_group_tx(self, **kwargs):
        t = Transaction("delete-group", ACTION_DATA(**kwargs))
        return t

    def make_delete_acl_tx(self, **kwargs):
        t = Transaction("delete-acl", ACTION_DATA(**kwargs))
        return t

    def make_delete_aci_tx(self, **kwargs):
        t = Transaction("delete-aci", ACTION_DATA(**kwargs))
        return t

    def make_delete_bobject_tx(self, **kwargs):
        t = Transaction("delete-bobject", ACTION_DATA(**kwargs))
        return t

    def check_update_bobject_tx(self, tx):
        domuid = tx.tdata.data['uid']
        return self.db0objects.get(domuid, False)

    def make_update_bobject_tx(self, **kwargs):
        t = Transaction("update-bobject", ACTION_DATA(**kwargs))
        return t

    def make_update_user_tx(self, **kwargs):
        t = Transaction("update-user", ACTION_DATA(**kwargs))
        return t

    def make_update_group_tx(self, **kwargs):
        t = Transaction("update-group", ACTION_DATA(**kwargs))
        return t

    def make_update_acl_tx(self, **kwargs):
        t = Transaction("update-acl", ACTION_DATA(**kwargs))
        return t

    def make_update_aci_tx(self, **kwargs):
        t = Transaction("update-aci", ACTION_DATA(**kwargs))
        return t

    def handle_tx(self, tx):
        """Handle transaction tx"""
        b = None
        if tx.ttype == "add-bdomain":
            b = self.check_add_bdomain_tx(tx)
            if b:
                b.uid = self.db0domains.incid + 1
                self.db0domains.set(b.uid, b)
                self.bdomainschain.add_transaction(tx)
                print("CREATE TRANSACTION ADDED SUCCESSFULLY")
        elif tx.ttype == "delete-bdomain":
            b = self.check_delete_bdomain_tx(tx)
            if b:
                self.bdomainschain.add_transaction(tx)
                del self.db0domains[tx.tdata.data['uid']]
                print("DELETE TRANSACTION ADDED SUCCESSFULLY")
        elif tx.ttype == "update-bdomain":
            b = self.check_update_bdomain_tx(tx)
            if b:
                for k,v in tx.tdata.data.items():
                    setattr(b, k, v)
                self.db0domains.update(b.uid, b)
                self.bdomainschain.add_transaction(tx)
                print("UPDATE TRANSACTION ADDED SUCCESSFULLY")
        elif tx.ttype in ["add-bobject", "add-user", "add-group", "add-acl", "add-aci"]:
            b = self.check_add_bobject_tx(tx)
            if b:
                b.uid = self.db0objects.incid + 1
                self.db0objects.set(b.uid, b)
                self.bobjectschain.add_transaction(tx)
                print("CREATE TRANSACTION ADDED SUCCESSFULLY")
                if tx.ttype == "add-user":
                    self.users[b.data['uid']] = b.data
                elif tx.ttype == "add-group":
                    self.groups[b.data['uid']] = b.data
                elif tx.ttype == "add-acl":
                    self.acls[b.data['uid']] = b.data
                elif tx.ttype == "add-aci":
                    self.acis[b.data['uid']] = b.data

        elif tx.ttype in ["delete-bobject", "delete-user", "delete-group", "delete-acl", "delete-aci"]:
            b = self.check_delete_bobject_tx(tx)
            if b:
                self.bobjectschain.add_transaction(tx)
                del self.db0objects[tx.tdata.data['uid']]
                if tx.ttype == "delete-user":
                    del self.users[b.data['uid']]
                elif tx.ttype == "delete-group":
                    del self.groups[b.data['uid']]
                elif tx.ttype == "delete-acl":
                    del self.acls[b.data['uid']]
                elif tx.ttype == "delete-aci":
                    del self.acis[b.data['uid']]
                print("DELETE TRANSACTION ADDED SUCCESSFULLY")
        elif  tx.ttype in ["update-bobject", "update-user", "update-group", "update-acl", "update-aci"]:
            b = self.check_update_bobject_tx(tx)
            if b:
                for k,v in tx.tdata.data.items():
                    setattr(b, k, v)
                self.db0objects.update(b.uid, b)
                self.bobjectschain.add_transaction(tx)
                print("UPDATE TRANSACTION ADDED SUCCESSFULLY")
                if tx.ttype == "update-user":
                    objid = b.data['uid']
                    obj = self.users[objid]
                    payload = b.data.items()
                    for k,v in payload:
                        obj[k] = v
                        self.users[objid] = obj
                elif tx.ttype == "update-group":
                    objid = b.data['uid']
                    obj = self.groups[objid]
                    payload = b.data.items()
                    for k,v in payload:
                        obj[k] = v
                        self.groups[objid] = obj
                elif tx.ttype == "update-acl":
                    objid = b.data['uid']
                    obj = self.groups[objid]
                    payload = b.data.items()
                    for k,v in payload:
                        obj[k] = v
                        self.groups[objid] = obj
                elif tx.ttype == "update-aci":
                    objid = b.data['uid']
                    obj = self.groups[objid]
                    payload = b.data.items()
                    for k,v in payload:
                        obj[k] = v
                        self.acis[objid] = obj
                    
        if hasattr(b, "uid"):
            return b.uid
        return False

    def summary(self):
        print("#USERS:", len(self.users))
        print(self.users)
        print("#GROUPS:", len(self.groups))
        print(self.groups)
        print("#ACLS:", len(self.acls))
        print(self.acls)
        print("#ACIS: ", len(self.acis))
        print(self.acis)

    def save_chains(self, filepath):
        """Save chain to a file"""
        chains = {'bdomainschain': self.bdomainschain, 'bobjectschain':self.bobjectschain}
        with open(filepath, "wb") as f:
            dump(chains, f)

    def load_chains(self, filepath):
        """Load chain from a file"""
        with open(filepath, "rb") as f:
            chains = load(f)
            self.bobjectschain = chains['bobjectschain']
            self.bdomainschain = chains['bdomainschain']


    def restore_state(self):
        for tx in self.bdomainschain:
            self.handle_tx(tx)
        for tx in self.bobjectschain:
            self.handle_tx(tx)
